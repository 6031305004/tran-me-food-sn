export default [
  {
    title: 'ขนมปัง + ไข่ดาว',
    image: 'https://cache.gmo2.sistacafe.com/images/uploads/summary/image/50197/c053d436372dc3bf8bc4c80f303a855d.jpg',
    cta: 'ดูข้อมูล', 
    horizontal: true
  },
  {
    title: 'น้ำปล่าว 1-2 แก้ว',
    image: 'https://s.isanook.com/he/0/ud/2/10793/drinking-water.jpg',
    cta: 'ดูข้อมูล', 
  },
  {
    image: 'https://sites.google.com/site/prayochncakkakkafae/_/rsrc/1422283633176/thima-laea-khwam-sakhay/kar-chng-kafae/117211960.jpg',
    cta: 'ดูข้อมูล', 
  },
  {
    title: 'ข้าวผัด หมู,ไก่,หมึก,กุ้ง',
    image: 'https://www.knorr.com/content/dam/unilever/global/recipe_image/117/11782/117820-default.jpg/_jcr_content/renditions/cq5dam.web.800.600.jpeg',
    cta: 'ดูข้อมูล', 
  },
  {
    title: 'ข้อแนะนำและรูปแบบที่เหมาะสม',
    image: 'https://www.hongthongrice.com/v2/wp-content/uploads/2017/05/HTR-food-category-5-Cover.jpg',
   
    horizontal: true
  },
];