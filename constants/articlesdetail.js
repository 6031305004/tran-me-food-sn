export default [
  {
    title: 'ไข่ดาว',
    image: 'https://cdn.shopify.com/s/files/1/0066/2791/7914/products/download_27.jpg?v=1585989646',
    info: '1 ฟอง\nแคลอรี่ (kcal) 165',
  },
  {
    title: 'ขนมปัง',
    image: 'https://i0.wp.com/kcgcorporation.com/wp-content/uploads/2009/12/Loaf-Bread-.jpg?fit=300%2C225&ssl=1',
    info: 'ปริมาณต่อ 100 g \nแคลอรี (kcal) 264',
  },
  {
    title: 'กาแฟ',
    image: 'https://sites.google.com/site/prayochncakkakkafae/_/rsrc/1422283633176/thima-laea-khwam-sakhay/kar-chng-kafae/117211960.jpg',
    info: 'ปริมาณต่อ 100 g \nแคลอรี (kcal) 0.5\n ',
    trie : 'Super Very Good!',
  },
  {
    title: 'ไข่ดาว',
    image: 'https://images.unsplash.com/photo-1487222477894-8943e31ef7b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1326&q=80',
    cta: 'ดูข้อมูล', 
  },
  {
    title: 'ข้อแนะนำและรูปแบบที่เหมาะสม',
    image: 'https://www.hongthongrice.com/v2/wp-content/uploads/2017/05/HTR-food-category-5-Cover.jpg',
   
    horizontal: true
  },
];