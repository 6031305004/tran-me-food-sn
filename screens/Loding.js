import React, { Component } from 'react'
import { Text, View, ActivityIndicator } from 'react-native'
import firebase from 'firebase'

export default class Loding extends Component {

    componentDidMount(){
        this.checkIfLoggedIn();
    }

    checkIfLoggedIn = () => {
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                this.props.navigation.navigate('Profile');
            }
            else{
                this.props.navigation.navigate('Onboarding');
            }
        }.bind(this)) 
    }

    render() {
        return (
            <View style={{flex : 1 , margin : 100 , marginTop : 250}}>
                <ActivityIndicator size="large"/>
            </View>
        )
    }
}
