import React from 'react';
import { StyleSheet, Dimensions, ScrollView, Text , FlatList, View} from 'react-native';
import { Block, theme } from 'galio-framework';

import { Cards } from '../components';
import articles from '../constants/articles';
const { width } = Dimensions.get('screen');
import { argonTheme, tabs } from "../constants";
import firebase, { database } from 'firebase';


class MenuFood extends React.Component {

    componentDidMount(){
      
      
    const data = firebase.database().ref('Food').once('value',(data) =>{
      console.log(data.toJSON())
    });

    
  
    }
    // onPressRecipe = item => {
    //   this.props.navigation.navigate('Recipe', { item });
    // };
    renderRecipes = ({ item }) => (
      <TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={() => this.onPressRecipe(item)}>
        <View style={styles.container}>
          <Image style={styles.photo} source={{ uri: item.pic }} />
          <Text style={styles.title}>{item.name}</Text>
          <Text style={styles.category}>{item.cal}</Text>
        </View>
      </TouchableHighlight>
    );

  render() {
    return (

      <ScrollView >
          
        <Block flex>
          
          <Text style={{fontSize: 30 , alignItems:'center' ,flex:1 ,margin : 10}}
          >เลือกอาหาร</Text>
      {/* <View>
        <FlatList
          vertical
          showsVerticalScrollIndicator={false}
          numColumns={2}
          data={data}
          renderItem={this.renderRecipes}
          keyExtractor={item => `${item.recipeId}`}
        />
      </View> */}

     
   
          
          
        </Block>


      </ScrollView>
    )
  }

 
}

const styles = StyleSheet.create({

  home: {
    width: width,    
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default MenuFood;
