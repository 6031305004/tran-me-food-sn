import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";


import firebase from 'firebase';

const { width, height } = Dimensions.get("screen");

class Login extends React.Component {

  state = {
    email : "",
    password : "",
    errorMassage : null
  }

  handleLogin = () => {
    const {email, password} = this.state
    firebase.auth().signInWithEmailAndPassword(email, password).then(function(user) {
      // user signed in
   }).catch(function(error) {
       var errorCode = error.code;
       var errorMessage = error.message;
   
       if (errorCode === 'auth/wrong-password') {
           alert('Wrong password.');
       } else {
           alert(errorMessage);         
       }
       console.log(error);
   });
  }


  render() {
    const { navigation } = this.props;
    return (
      <Block flex middle>
        <StatusBar hidden />
        <ImageBackground
          source={Images.RegisterBackground}
          style={{ width, height, zIndex: 1 }}
        >
          <Block flex middle>
            <Block style={styles.registerContainer}>
              <Block flex>
                <Block flex={0.17} middle>
                  <Text color="#8898AA" size={12}>
                    เข้าสู่ระบบ
                  </Text>
                </Block>
                <Block flex center>
                  <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior="padding"
                    enabled
                  >
                    <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                      <Input
                        borderless
                        placeholder="อีเมล"
                        onChangeText={email => this.setState({ email })}
                        value = {this.state.email}
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="ic_mail_24px"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                    </Block>
                    <Block width={width * 0.8}>
                      <Input
                        password
                        borderless
                        placeholder="รหัสผ่าน"
                        onChangeText={password => this.setState({ password })}
                        value = {this.state.password}
                        iconContent={
                          <Icon
                            size={16}
                            color={argonTheme.COLORS.ICON}
                            name="padlock-unlocked"
                            family="ArgonExtra"
                            style={styles.inputIcons}
                          />
                        }
                      />
                     
                    </Block>
                   <Block>
                     {this.state.errorMassage && <Text style={{color :"#FF0000"}}>{this.state.errorMassage} </Text>}
                   </Block>
                      <Block middle>
                      <Button color="primary" style={styles.createButton}
                       onPress={this.handleLogin}
                      //  onPress={() => navigation.navigate("App")}
                       >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          เข้าสู่ระบบ
                          
                        </Text>
                      </Button>
                    </Block>
                    <Block middle>
                  <Button color="primary" style={styles.createButton}
                    onPress={() => navigation.goBack()}>
                    <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                      ย้อนกลับ
                        </Text>
                  </Button>
                  
                </Block>
                  </KeyboardAvoidingView>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

export default Login;
