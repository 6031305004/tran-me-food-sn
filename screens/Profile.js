// import React from "react";
// import {
//   StyleSheet,
//   Dimensions,
//   ScrollView,
//   Image,
//   ImageBackground,
//   Platform
// } from "react-native";
// import { Block, Text, theme } from "galio-framework";

// import { Button } from "../components";
// import { Images, argonTheme } from "../constants";
// import { HeaderHeight } from "../constants/utils";

// const { width, height } = Dimensions.get("screen");

// const thumbMeasure = (width - 48 - 32) / 3;

// class Profile extends React.Component {
//   render() {
//     return (
//       <Block flex style={styles.profile}>
//         <Block flex>
//           <ImageBackground
//             source={Images.ProfileBackground}
//             style={styles.profileContainer}
//             imageStyle={styles.profileBackground}
//           >
//             <ScrollView
//               showsVerticalScrollIndicator={false}
//               style={{ width, marginTop: '25%' }}
//             >
//               <Block flex style={styles.profileCard}>
//                 <Block middle style={styles.avatarContainer}>
//                   <Image
//                     source={{ uri: Images.ProfilePicture }}
//                     style={styles.avatar}
//                   />
//                 </Block>
//                 <Block style={styles.info}>
//                   <Block
//                     middle
//                     row
//                     space="evenly"
//                     style={{ marginTop: 20, paddingBottom: 24 }}
//                   >
//                     <Button
//                       small
//                       style={{ backgroundColor: argonTheme.COLORS.INFO }}
//                     >
//                       CONNECT
//                     </Button>
//                     <Button
//                       small
//                       style={{ backgroundColor: argonTheme.COLORS.DEFAULT }}
//                     >
//                       MESSAGE
//                     </Button>
//                   </Block>
//                   <Block row space="between">
//                     <Block middle>
//                       <Text
//                         bold
//                         size={18}
//                         color="#525F7F"
//                         style={{ marginBottom: 4 }}
//                       >
//                         2K
//                       </Text>
//                       <Text size={12} color={argonTheme.COLORS.TEXT}>Orders</Text>
//                     </Block>
//                     <Block middle>
//                       <Text
//                         bold
//                         color="#525F7F"
//                         size={18}
//                         style={{ marginBottom: 4 }}
//                       >
//                         10
//                       </Text>
//                       <Text size={12} color={argonTheme.COLORS.TEXT}>Photos</Text>
//                     </Block>
//                     <Block middle>
//                       <Text
//                         bold
//                         color="#525F7F"
//                         size={18}
//                         style={{ marginBottom: 4 }}
//                       >
//                         89
//                       </Text>
//                       <Text size={12} color={argonTheme.COLORS.TEXT}>Comments</Text>
//                     </Block>
//                   </Block>
//                 </Block>
//                 <Block flex>
//                   <Block middle style={styles.nameInfo}>
//                     <Text bold size={28} color="#32325D">
//                       Jessica Jones, 27
//                     </Text>
//                     <Text size={16} color="#32325D" style={{ marginTop: 10 }}>
//                       San Francisco, USA
//                     </Text>
//                   </Block>
//                   <Block middle style={{ marginTop: 30, marginBottom: 16 }}>
//                     <Block style={styles.divider} />
//                   </Block>
//                   <Block middle>
//                     <Text
//                       size={16}
//                       color="#525F7F"
//                       style={{ textAlign: "center" }}
//                     >
//                       An artist of considerable range, Jessica name taken by
//                       Melbourne …
//                     </Text>
//                     <Button
//                       color="transparent"
//                       textStyle={{
//                         color: "#233DD2",
//                         fontWeight: "500",
//                         fontSize: 16
//                       }}
//                     >
//                       Show more
//                     </Button>
//                   </Block>
//                   <Block
//                     row
//                     style={{ paddingVertical: 14, alignItems: "baseline" }}
//                   >
//                     <Text bold size={16} color="#525F7F">
//                       Album
//                     </Text>
//                   </Block>
//                   <Block
//                     row
//                     style={{ paddingBottom: 20, justifyContent: "flex-end" }}
//                   >
//                     <Button
//                       small
//                       color="transparent"
//                       textStyle={{ color: "#5E72E4", fontSize: 12 }}
//                     >
//                       View all
//                     </Button>
//                   </Block>
//                   <Block style={{ paddingBottom: -HeaderHeight * 2 }}>
//                     <Block row space="between" style={{ flexWrap: "wrap" }}>
//                       {Images.Viewed.map((img, imgIndex) => (
//                         <Image
//                           source={{ uri: img }}
//                           key={`viewed-${img}`}
//                           resizeMode="cover"
//                           style={styles.thumb}
//                         />
//                       ))}
//                     </Block>
//                   </Block>
//                 </Block>
//               </Block>
//             </ScrollView>
//           </ImageBackground>
//         </Block>
//         {/* <ScrollView showsVerticalScrollIndicator={false} 
//                     contentContainerStyle={{ flex: 1, width, height, zIndex: 9000, backgroundColor: 'red' }}>
//         <Block flex style={styles.profileCard}>
//           <Block middle style={styles.avatarContainer}>
//             <Image
//               source={{ uri: Images.ProfilePicture }}
//               style={styles.avatar}
//             />
//           </Block>
//           <Block style={styles.info}>
//             <Block
//               middle
//               row
//               space="evenly"
//               style={{ marginTop: 20, paddingBottom: 24 }}
//             >
//               <Button small style={{ backgroundColor: argonTheme.COLORS.INFO }}>
//                 CONNECT
//               </Button>
//               <Button
//                 small
//                 style={{ backgroundColor: argonTheme.COLORS.DEFAULT }}
//               >
//                 MESSAGE
//               </Button>
//             </Block>

//             <Block row space="between">
//               <Block middle>
//                 <Text
//                   bold
//                   size={12}
//                   color="#525F7F"
//                   style={{ marginBottom: 4 }}
//                 >
//                   2K
//                 </Text>
//                 <Text size={12}>Orders</Text>
//               </Block>
//               <Block middle>
//                 <Text bold size={12} style={{ marginBottom: 4 }}>
//                   10
//                 </Text>
//                 <Text size={12}>Photos</Text>
//               </Block>
//               <Block middle>
//                 <Text bold size={12} style={{ marginBottom: 4 }}>
//                   89
//                 </Text>
//                 <Text size={12}>Comments</Text>
//               </Block>
//             </Block>
//           </Block>
//           <Block flex>
//               <Block middle style={styles.nameInfo}>
//                 <Text bold size={28} color="#32325D">
//                   Jessica Jones, 27
//                 </Text>
//                 <Text size={16} color="#32325D" style={{ marginTop: 10 }}>
//                   San Francisco, USA
//                 </Text>
//               </Block>
//               <Block middle style={{ marginTop: 30, marginBottom: 16 }}>
//                 <Block style={styles.divider} />
//               </Block>
//               <Block middle>
//                 <Text size={16} color="#525F7F" style={{ textAlign: "center" }}>
//                   An artist of considerable range, Jessica name taken by
//                   Melbourne …
//                 </Text>
//                 <Button
//                   color="transparent"
//                   textStyle={{
//                     color: "#233DD2",
//                     fontWeight: "500",
//                     fontSize: 16
//                   }}
//                 >
//                   Show more
//                 </Button>
//               </Block>
//               <Block
//                 row
//                 style={{ paddingVertical: 14, alignItems: "baseline" }}
//               >
//                 <Text bold size={16} color="#525F7F">
//                   Album
//                 </Text>
//               </Block>
//               <Block
//                 row
//                 style={{ paddingBottom: 20, justifyContent: "flex-end" }}
//               >
//                 <Button
//                   small
//                   color="transparent"
//                   textStyle={{ color: "#5E72E4", fontSize: 12 }}
//                 >
//                   View all
//                 </Button>
//               </Block>
//               <Block style={{ paddingBottom: -HeaderHeight * 2 }}>
//                 <Block row space="between" style={{ flexWrap: "wrap" }}>
//                   {Images.Viewed.map((img, imgIndex) => (
//                     <Image
//                       source={{ uri: img }}
//                       key={`viewed-${img}`}
//                       resizeMode="cover"
//                       style={styles.thumb}
//                     />
//                   ))}
//                 </Block>
//               </Block>
//           </Block>
//         </Block>
//                   </ScrollView>*/}
//       </Block>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   profile: {
//     marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
//     // marginBottom: -HeaderHeight * 2,
//     flex: 1
//   },
//   profileContainer: {
//     width: width,
//     height: height,
//     padding: 0,
//     zIndex: 1
//   },
//   profileBackground: {
//     width: width,
//     height: height / 2
//   },
//   profileCard: {
//     // position: "relative",
//     padding: theme.SIZES.BASE,
//     marginHorizontal: theme.SIZES.BASE,
//     marginTop: 65,
//     borderTopLeftRadius: 6,
//     borderTopRightRadius: 6,
//     backgroundColor: theme.COLORS.WHITE,
//     shadowColor: "black",
//     shadowOffset: { width: 0, height: 0 },
//     shadowRadius: 8,
//     shadowOpacity: 0.2,
//     zIndex: 2
//   },
//   info: {
//     paddingHorizontal: 40
//   },
//   avatarContainer: {
//     position: "relative",
//     marginTop: -80
//   },
//   avatar: {
//     width: 124,
//     height: 124,
//     borderRadius: 62,
//     borderWidth: 0
//   },
//   nameInfo: {
//     marginTop: 35
//   },
//   divider: {
//     width: "90%",
//     borderWidth: 1,
//     borderColor: "#E9ECEF"
//   },
//   thumb: {
//     borderRadius: 4,
//     marginVertical: 4,
//     alignSelf: "center",
//     width: thumbMeasure,
//     height: thumbMeasure
//   }
// });

// export default Profile;


import React from 'react';
import firebase from 'firebase'
import { StyleSheet, Dimensions, ScrollView, Image, ImageBackground, Platform,  } from 'react-native';
import { Block, Text, theme, Button, } from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';

import { Icon } from '../components';
import { Images, materialTheme } from '../constants';
import { HeaderHeight } from "../constants/utils";

import argonTheme from "../constants/Theme";
const { width, height } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

export default class Profile extends React.Component {

  constructor(props){
    super(props);
    this.state  = {
      U_Weight: []
    }
  }

  componentDidMount(){
    const rootRef = firebase.database().ref().child('User_ID');
    const rootID = rootRef.child('0');
    const nameRef = rootID.child('U_Fname');
    const lnameRef = rootID.child('U_Lname');
    const styleRef = rootID.child('U_Style');
    const weightRef = rootID.child('U_Weight');
    const duraRef = rootID.child('U_Dura');
    const chalRef = rootID.child('U_Chal');
    const wnRef = rootID.child('U_Wn');
    const picRef = rootID.child('U_Pic');


    nameRef.on('value', snap =>{
      this.setState({
        U_Fname: snap.val()
      });
    });
    lnameRef.on('value', snap =>{
      this.setState({
        U_Lname: snap.val()
      });
    });
    styleRef.on('value', snap =>{
      this.setState({
        U_Style: snap.val()
      });
    });
    weightRef.on('value', snap =>{
      this.setState({
        U_Weight: snap.val()
      });
    });
    duraRef.on('value', snap =>{
      this.setState({
        U_Dura: snap.val()
      });
    });
    chalRef.on('value', snap =>{
      this.setState({
        U_Chal: snap.val()
      });
    });
    wnRef.on('value', snap =>{
      this.setState({
        U_Wn: snap.val()
      });
    });
    picRef.on('value', snap =>{
      this.setState({
        U_Pic: snap.val()
      });
    });
  }

  render() {
    const { navigation } = this.props;
    return (

      <Block flex style={styles.profile}>
        <Block flex>
          <ImageBackground
            source={{uri:this.state.U_Pic}}
            style={styles.profileContainer}
            imageStyle={styles.profileImage}>
         
            <Block flex style={styles.profileDetails}>
              
              <Block style={styles.profileTexts}>
               {this.props.message}
    <Text color="white" size={28} style={{ paddingBottom: 8 }}>{this.state.U_Fname} {this.state.U_Lname}</Text>
               
               
                <Block row space="between">
                  <Block row>
                    <Block middle style={styles.pro}>
    <Text size={16} color="white">{this.state.U_Style}</Text>
                    </Block>
                    <Block>
                  <Button
                  style={styles.buttonpro}
                  color={argonTheme.COLORS.SUCCESS}
                 onPress={() => navigation.navigate("Style")}
                  textStyle={{ color: argonTheme.COLORS.BLACK }}
                >
                  เลือกรูปแบบ
                </Button>
                  </Block>
  
                  
                </Block>
                
                
              </Block>
              
              <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']} style={styles.gradient} />
            </Block>
            </Block>
                  


          </ImageBackground>
        </Block>
        <Block flex style={styles.options}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Block row space="between" style={{ padding: theme.SIZES.BASE, }}>
              <Block middle>
                  <Text bold size={12} style={{marginBottom: 8}}>{this.state.U_Weight}</Text>
                <Text muted size={12}>น้ำหนัก</Text>
              </Block>
              <Block middle>
                  <Text bold size={12} style={{marginBottom: 8}}>{this.state.U_Dura} วัน</Text>
                <Text muted size={12}>ระยะเวลา</Text>
              </Block>
              <Block middle>
                  <Text bold size={12} style={{marginBottom: 8}}>{this.state.U_Chal}</Text>
                <Text muted size={12}>เป้าหมาย</Text>
              </Block>
            </Block>
            <Block row space="between" style={{ padding: theme.SIZES.BASE, }}>
              <Block middle>
    <Text bold size={12} style={{marginBottom: 8}}>{this.state.U_Wn}</Text>
                <Text muted size={12}>น้ำหนักล่าสุด</Text>
              </Block>
              <Block middle>
                <Text bold size={12} style={{marginBottom: 8}}>- วัน</Text>
                <Text muted size={12}>เาลาที่เหลือ</Text>
              </Block>
              <Block middle>
                <Text bold size={12} style={{marginBottom: 8}}>- %</Text>
                <Text muted size={12}>ความสำเร็จ</Text>
              </Block>
            </Block>
            <Block row space="between" style={{ paddingVertical: 16, alignItems: 'baseline' }}>
              <Text size={16}>Recently viewed</Text>
              {/* <Text size={12} color={theme.COLORS.PRIMARY} onPress={() => this.props.navigation.navigate('Home')}>View All</Text> */}
            </Block>

                 
            <Block center>
                <Button
                  style={styles.button}
                  color={argonTheme.COLORS.SUCCESS}
                 onPress={() => navigation.navigate("Home")}
                  textStyle={{ color: argonTheme.COLORS.BLACK }}
                >
                  ดูเมนูอาหารวันนี้
                </Button>
                <Button
                  style={styles.button}
                  color={argonTheme.COLORS.GRADIENT_END}
                // onPress={() => navigation.navigate("MainNavigator")}
                  textStyle={{ color: argonTheme.COLORS.BLACK }}
                >
                  ออกกำลังกาย
                </Button>

                <Button
                  style={styles.button}
                  color={argonTheme.COLORS.SWITCH_OFF}
                 onPress={() => firebase.auth().signOut()}
                  textStyle={{ color: argonTheme.COLORS.BLACK }}
                >
                  ออกจากระบบ
                  
                </Button>

                
              </Block>






            {/* <Block style={{ paddingBottom: -HeaderHeight * 2 }}> */}
              {/* <Block row space="between" style={{ flexWrap: 'wrap' }} >
                {Images.Viewed.map((img, imgIndex) => (
                  <Image
                    source={{ uri: img }}
                    key={`viewed-${img}`}  
                    resizeMode="cover"
                    style={styles.thumb}
                  />
                ))}
              </Block> */}
            {/* </Block> */}
          </ScrollView>
        </Block>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
    marginBottom: -HeaderHeight * 2,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
    marginTop : 20
  },
  buttonpro: {
    height: 25,
    width: 100,
    shadowRadius: 0,
    shadowOpacity: 0,
    marginLeft: 100,
  },
  profileImage: {
    width: width * 1.1,
    height: 'auto',
  },
  profileContainer: {
    width: width,
    height: height / 2,
  },
  profileDetails: {
    paddingTop: theme.SIZES.BASE * 4,
    justifyContent: 'flex-end',
    position: 'relative',
    
  },
  profileTexts: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
    zIndex: 2
  },
  pro: {
    backgroundColor: materialTheme.COLORS.LABEL,
    paddingHorizontal: 6,
    marginRight: theme.SIZES.BASE / 2,
    borderRadius: 4,
    height: 25,
    width: 100,
  },
  seller: {
    marginRight: theme.SIZES.BASE / 2,
  },
  options: {
    position: 'relative',
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -theme.SIZES.BASE * 7,
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
  gradient: {
    zIndex: 1,
    left: 0,
    right: 0,
    bottom: 0,
    height: '50%',
    position: 'absolute',
  },
});
