import React from "react";
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  StatusBar,
  TouchableOpacity,
 
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { Button, Icon, Input } from "../components";
import { Images, argonTheme } from "../constants";

const { width, height } = Dimensions.get("screen");

class Style extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <Block flex middle>
        <StatusBar hidden />
        <ImageBackground
          source={Images.RegisterBackground}
          style={{ width, height, zIndex: 1 }}
        >
          <Block flex middle>
            <Block style={styles.registerContainer}>
              <Block flex>
                <Block flex={0.17} middle>
                  <Text color="#8898AA" size={12}>
                    เลือกรูปแบบ
                  </Text>
                </Block>
                <Block flex center>
                 
                    <Block middle>
                    <TouchableOpacity>
                      <Button color="primary" style={{width: width * 0.7,height : 100 ,marginTop: 25 ,       
                    }}
                       onPress={() => navigation.navigate("MenuFood")}
                       >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          เพิ่มน้ำหนัก 
                        </Text>
                        
                      </Button>
                      </TouchableOpacity>

                      <Button color="primary" style={styles.createButton}
                       onPress={() => navigation.navigate("MenuFood")}
                       >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                          ลดน้ำหนัก
                        </Text>
                      </Button>



                      <Button color="primary" style={styles.createButton}
                       onPress={() => navigation.navigate("MenuFood")}
                       >
                        <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                        รักษาน้ำหนัก
                        </Text>
                      </Button>



                    </Block>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.78,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
 
  createButton: {
    width: width * 0.7,
    height : 100 ,
    marginTop: 25
  }
});

export default Style;
